
const BEST_ROW_COLOR = 'green';

const scores = [0, 0, 0, 0, 0, 0, // ignore these
	10000, // 6
	36, // 7, etc...
	720,
	360,
	80,
	252,
	108,
	72,
	54,
	180,
	72,
	180,
	119,
	36,
	306,
	1080,
	144,
	1800,
	3600
];

function compute() {
	// get input grid into memory
	const grid = Array(3).fill(null).map(() => Array(3).fill(0)); // 3x3 of zeroes
	const known_numbers = Array(9).fill(false);
	for (let x = 0; x < 3; x++) {
		for (let y = 0; y < 3; y++) {
			let el = document.getElementById(`${x+1}-${y+1}`);
			el.classList.remove(BEST_ROW_COLOR);
			const n = Number(el.value || 0);
			grid[x][y] = n;

			// mark known numbers
			if (n > 0) {
				known_numbers[n - 1] = true;
			}
		}
	}
	
	const line_coords = [
		// horizontal lines
		[ [0,0], [0,1], [0,2] ],
		[ [1,0], [1,1], [1,2] ],
		[ [2,0], [2,1], [2,2] ],

		// vertical lines
		[ [0,0], [1,0], [2,0] ],
		[ [0,1], [1,1], [2,1] ],
		[ [0,2], [1,2], [2,2] ],

		// down-right diagonal
		[ [0,0], [1,1], [2,2] ],

		// down-left diagonal
		[ [2,0], [1,1], [0,2] ],
	];

	// gather all 8 possible "lines"
	const lines = line_coords.map(line => line.map(coords => grid[coords[0]][coords[1]]));

	// list of unknown numbers
	const unknown_numbers = known_numbers.map((n, i) => n ? -1 : i + 1).filter(i => i > 0);
	// console.log('unknown numbers:', unknown_numbers);

	const combos = [
		combinationsAV2(5, 1),
		combinationsAV2(5, 2),
		combinationsAV2(5, 3),
	].map(comboset => comboset.map(combo => combo.map(n => n - 1)));
	
	// console.log('lines:', lines);
	// console.log('combos:', combos);

	const average_line_scores = Array(lines.length).fill(0);

	for (let i = 0; i < lines.length; i++) {
		// permute unknown numbers and find all possible sums
		let line_known_sum = 0;
		let line_unknown_numbers = 0;
		for (let j = 0; j < lines[i].length; j++) {
			line_known_sum += lines[i][j];
			if (lines[i][j] == 0) {
				line_unknown_numbers++;
			}
		}
		// console.log("line unknown:", line_unknown_numbers);

		if (line_unknown_numbers > 0) {
			const line_combos = combos[line_unknown_numbers - 1];
			const known_line_sum = getSum(lines[i]);
			// console.log("line sum:", known_line_sum);
			const possible_line_sums = Array(line_combos.length).fill(known_line_sum);
			// console.log("possible line sums:", lines[i], possible_line_sums);

			// n choose k of unknown numbers, sum with known
			for (let k = 0; k < line_combos.length; k++) {
				for (let l = 0; l < line_combos[k].length; l++) {
					const unknown_number_index = line_combos[k][l];
					possible_line_sums[k] += unknown_numbers[unknown_number_index];
					// console.log('guess:', unknown_number_index, unknown_numbers[unknown_number_index]);
				}
			}

			// console.log(`line ${i}:`, possible_line_sums);
			const possible_line_scores = possible_line_sums.map(sum => scores[sum]);
			average_line_scores[i] = getSum(possible_line_scores) / possible_line_scores.length;
		}
		else {
			average_line_scores[i] = scores[line_known_sum];
		}
	}

	// console.log(`averages:`, average_line_scores);
	const best_average_line = average_line_scores.indexOf(getMax(average_line_scores));
	// console.log(`best average line:`, best_average_line);

	best_average_line_coords = line_coords[best_average_line];
	// console.log(`best average line coords:`, best_average_line_coords);

	for (let i = 0; i < best_average_line_coords.length; i++) {
		const x = best_average_line_coords[i][0];
		const y = best_average_line_coords[i][1];
		document.getElementById(`${x+1}-${y+1}`).classList.add(BEST_ROW_COLOR);
	}
}

const getMax = (arr) => arr.reduce((acc, item) => (item > acc ? item : acc), arr[0]);
const getSum = (arr) => arr.reduce((a, b) => a + b, 0);

function combinationsAV2(n, k) {
	const result = [];
	const combos = [];
	const recurse = start => {
		if (combos.length + (n - start + 1) < k) { return }

		recurse(start + 1);
		combos.push(start);

		if (combos.length === k) {     
			result.push(combos.slice());
		}
		else if (combos.length + (n - start + 2) >= k) {
			recurse(start + 1);
		}

		combos.pop();     
	}

	recurse(1, combos);
	return result;
}